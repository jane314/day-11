// #DAY11 by JANE DAVIS <3 
// DECEMBER 2020.
// Convention: ALL CAPS for const vars' names


/*
 *
 GLOBAL VARIABLES
 *
 */

const App = {

el: '#app',

	data() {
			return 	{
				drawselection: "Drawing L's",
				colordot: 'BlanchedAlmond',
				colorpound: 'HotPink',
				colorL: 'LightBlue',
				rulescode: 'Rules are specified here in the following CSV format:\nA,B,C\nWhere:\nA = Type of Seat ( nochange, #, L, or . )\nB = # of Occupied Adjacent Seats\nC = Type Of Seat To Iterate To If Type Of Seat And Number Match Previous Two\nInvalid Lines Are Automatically Comments!\nSee Below Here For How To Set The Default Type Of Seat If No Rule Applies\n------------------\nEXAMPLE RULES:\nL,0,#\n#,4,L\n#,5,L\n#,6,L\n#,7,L\n#,8,L\ndefault,nochange'
			};
	},

	mounted() {

		this.CANVAS = document.getElementById("canvas");
		this.CTX = canvas.getContext("2d");
		this.COLOR = [ 'LightBlue', 'HotPink', 'BlanchedAlmond' ];
		this.MAX_COORDS = { X: 500, Y: 500 };
		this.SIDE_LEN = { X: 25, Y: 25 };
		this.MAX_INDEX = { X: this.MAX_COORDS.X / this.SIDE_LEN.X , Y: this.MAX_COORDS.Y / this.SIDE_LEN.Y };
		this.CANVAS.width = this.MAX_COORDS.X;
		this.CANVAS.height = this.MAX_COORDS.Y;
		this.iter = [];
		this.iter2 = [];
		this.rules = {};

		// init iters
		for( let j = 0; j < this.MAX_INDEX.Y; j++ ) {
				this.iter.push( [] );
				this.iter2.push( [] );
				for( let i = 0; i < this.MAX_INDEX.X; i++ ) {
						this.iter[j].push( '.' );
						this.iter2[j].push( '.' );
				}
		}
		
		this.randomize();
		
	},

	methods: { 
	
		pentool(e) { 
			
			let x = e.pageX;
			let y = e.pageY;
			x -= this.CANVAS.offsetLeft;
			y -= this.CANVAS.offsetTop;
			
			let mystatus = this.drawselection.replace("Drawing ","").replace("'s","");
			indexx = Math.floor( x / this.SIDE_LEN.X );
			indexy = Math.floor( y / this.SIDE_LEN.Y );
			this.iter[indexy][indexx] = mystatus;
			let squarex = indexx * this.SIDE_LEN.X;
			let squarey = indexy * this.SIDE_LEN.Y;
			let color;
			if( mystatus == '#' ) {
				color = this.colorpound;
			} else if( mystatus == 'L' ) {
				color = this.colorL;
			} else {
				color = this.colordot;
			}
			
			this.draw_rect( squarex, squarey, this.SIDE_LEN.X, this.SIDE_LEN.Y, color );
			
		},

		 // random int [0, n)
		 randi(n) {
				 return Math.floor(n * Math.random());
		 },

		 // random color
		 rand_color() {
				 return this.COLOR[ this.randi(3) ];
		 },

		 rand_status() {
				 return [ '.', 'L', '#' ][ this.randi(3) ];
		 },

		 color( x, y ) {
			let tmp = this.iter[ y / this.SIDE_LEN.Y ][ x / this.SIDE_LEN.X ];
			if( tmp == 'L' ) { return this.colorL; }
			if( tmp == '#' ) { return this.colorpound }
			if( tmp == '.' ) { return this.colordot; }
			return 'Black';
		 },

		 adjacent_coords( x, y ) {
				 const x_arr = [ x ];
				 const y_arr = [ x ];
				 const arr = [];
				 if( x - 1 >= 0 ) { x_arr.push( x - 1 ); }
				 if( y - 1 >= 0 ) { y_arr.push( y - 1 ); }
				 if( x + 1 < this.MAX_INDEX.X ) { x_arr.push( x + 1 ); }
				 if( y + 1 < this.MAX_INDEX.Y ) { y_arr.push( y + 1 ); }
				 x_arr.forEach( a => {
								 y_arr.forEach( b => {
												 arr.push( { X: a, Y: b } );
												 } )	} );
				 return arr;
		 },

		 occupied_adjacent( x, y )  {
				 return this.adjacent_coords( x, y ).map( pos => Number( this.iter[pos.Y][pos.X] == '#' ) ).reduce( (a,b) => a+b );
		 },

		 gen_cell( x, y ) {
				 let num =  this.occupied_adjacent( x, y );
				 if( this.rules[ this.iter[y][x] ].hasOwnProperty( num ) ) {
					 return this.rules[ this.iter[y][x] ][ num ];
				 }
				 return this.rules['default'];
		 },

		 gen() {
			 	this.update_rules();
				 for( let j = 0; j < this.MAX_INDEX.Y; j++ ) {
						 for( let i = 0; i < this.MAX_INDEX.X; i++ ) {
								let tmp =  this.gen_cell( i, j );
								if ( tmp.search('nochange') != -1 ) {
									this.iter2[j][i] = this.iter[j][i];
								} else {
									this.iter2[j][i] = tmp;
								}
						 }
				 }
				 this.iter = JSON.parse(JSON.stringify(this.iter2));
				 this.draw_chessboard();
		 },

		 /*
		  *
		  DRAWING FUNCTIONS
		  *
		  */

		 // draw a rectangle
		 draw_rect(x, y, x_len, y_len, color) {
				 this.CTX.beginPath();
				 this.CTX.rect(x, y, x_len, y_len);
				 this.CTX.fillStyle = color;
				 this.CTX.fill();
		 },

		 // draw chessboard
		 draw_chessboard( ) {
			 for( let x = 0; x < this.MAX_COORDS.X; x += this.SIDE_LEN.X ) {
					 for( let y = 0; y < this.MAX_COORDS.Y; y += this.SIDE_LEN.Y ) {
							 this.draw_rect( x, y, this.SIDE_LEN.X, this.SIDE_LEN.Y, this.color( x, y ) );
					 }
			 }
		 },
		 
		 update_rules() {
			this.rules = { 'L': {}, '.': {}, '#': {}, 'default': '.' }; 
			this.rulescode.split('\n').filter( str => str.length != 0 )
				.filter( str => ( str[0] == '#' || str[0] == '.' || str[0] == 'L' ) )
				.forEach( str => {
					let tmp = str.split(',');
					if( tmp[0] == 'default' ) {
						this.rules[tmp[0]] = tmp[1];
					} else {
						this.rules[tmp[0]][tmp[1]] = tmp[2];
					}
				} ); 
		 },
		 
		 randomize() { 
			for( let j = 0; j < this.MAX_INDEX.Y; j++ ) {
				for( let i = 0; i < this.MAX_INDEX.X; i++ ) {
					this.iter[j][i] = this.rand_status();
				}
			}
			this.draw_chessboard();
		 }

	}

};

const app = Vue.createApp( App ).mount('#root');
app.draw_chessboard();
